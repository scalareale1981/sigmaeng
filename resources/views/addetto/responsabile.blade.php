@extends('layouts.dashboardAddetto')

@section('page_heading','Responsabile '.Auth::user()->nome.' '.Auth::user()->cognome)
@section('section')
    <div class="col-sm-12">
        <div class="table-responsive">

            <table class="table table-striped">
                <tr>
                    <th>
                        Denominazione
                    </th>
                    <th>
                        Indirizzo
                    </th>
                    <th>
                        Referente
                    </th>
                    <th>

                    </th>
                </tr>
                @foreach($responsabilita as $value)
                    <tr>
                        <td>
                            {{$value->denominazione}}
                        </td>
                        <td>
                            {{$value->indirizzo}}
                            {{$value->citta}}
                            {{$value->provincia}}
                        </td>
                        <td>
                            @if($value->referente)
                                {{$value->referente->nome}} {{$value->referente->cognome}}
                            @endif
                        </td>
                        <td>
                            @if($value->records->count())
                                <i class="fa fa-arrow-circle-right btn btn-info" onclick="$('#row_{{$value->id}}').toggle(); $(this).toggleClass('fa-rotate-90');"></i>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="display: none;" id="row_{{$value->id}}">
                            @if($value->records->count())
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>Sindacato</th>
                                            <th>Federazione</th>
                                            <th>Codice sede</th>
                                            <th>Sede in chiaro</th>
                                            <th>Tipo presentazione</th>
                                            <th>Cognome</th>
                                            <th>Nome</th>
                                            <th>Codice fiscale</th>
                                            <th>Data nascita</th>
                                            <th>Importo trattenuta</th>
                                            <th>Data valuta</th>
                                            <th>Data pagamento</th>
                                            <th>Numero domande</th>
                                            <th>Comune residenza</th>
                                            <th>Codice patronato</th>
                                            <th>Ufficio patronato</th>
                                            <th>Patronato</th>
                                            <th>Quota</th>
                                        </tr>
                                        @foreach($value->records as $record)
                                            <tr>
                                                <td>{{$record->sindacato}}</td>
                                                <td>{{$record->federazione}}</td>
                                                <td>{{$record->Codice_sede}}</td>
                                                <td>{{$record->sede_in_chiaro}}</td>
                                                <td>{{$record->tipo_presentazione}}</td>
                                                <td>{{$record->cognome}}</td>
                                                <td>{{$record->nome}}</td>
                                                <td>{{$record->codice_fiscale}}</td>
                                                <td>{{$record->data_nascita->format("d/m/Y")}}</td>
                                                <td>{{$record->importo_trattenuta}}</td>
                                                <td>{{$record->data_valuta->format("d/m/Y")}}</td>
                                                <td>{{$record->data_pagamento->format("d/m/Y")}}</td>
                                                <td>{{$record->numero_domande}}</td>
                                                <td>{{$record->comune_residenza}}</td>
                                                <td>{{$record->codice_patronato}}</td>
                                                <td>{{$record->ufficio_patronato}}</td>
                                                <td>{{$record->patronato}}</td>
                                                <td>{!! ($record->importo_trattenuta * $value->responsabile_quota)/100!!}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    <div class="col-12" style="padding-left: 20px;" >
        {{$responsabilita->links()}}
    </div>
@stop
