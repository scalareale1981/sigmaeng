<input type="hidden" name="category_id" value="2">

<div class="form-group">
    <label for="nome">Nome</label>
    <input type="text" class="form-control" value="{{old('nome', !empty($user->nome) ? $user->nome : '')}}"  id="nome" name="nome">
    @if($errors->has('nome'))
        <span class="text-danger" >
            {{ $errors->first('nome') }}
        </span>
    @endif
</div>

<div class="form-group">
    <label for="nome">Cognome</label>
    <input type="text" class="form-control" value="{{old('cognome', !empty($user->cognome) ? $user->cognome : '')}}"  id="cognome" name="cognome">
    @if($errors->has('cognome'))
        <span class="text-danger" >
            {{ $errors->first('cognome') }}
        </span>
    @endif
</div>

<div class="form-group">
    <label for="nome">Data di Nascita</label>
    <input type="date" class="form-control" value="{{old('data_di_nascita', !empty($user->data_di_nascita) ? $user->data_di_nascita->format('Y-m-d') : '')}}"  id="data_di_nascita" name="data_di_nascita">
    @if($errors->has('data_di_nascita'))
        <span class="text-danger" >
            {{ $errors->first('data_di_nascita') }}
        </span>
    @endif
</div>

<div class="form-group">
    <label for="codice_fiscale">Codice Fiscale</label>
    <input type="text" class="form-control" value="{{old('codice_fiscale', !empty($user->codice_fiscale) ? $user->codice_fiscale : '')}}"  id="codice_fiscale" name="codice_fiscale">
    @if($errors->has('codice_fiscale'))
        <span class="text-danger" >
            {{ $errors->first('codice_fiscale') }}
        </span>
    @endif
</div>

<div class="form-group">
    <label for="codice">Codice</label>
    <input type="text" class="form-control" value="{{old('codice', !empty($user->codice) ? $user->codice : '')}}" id="codice" name="codice">
    @if($errors->has('codice'))
        <span class="text-danger" >
            {{ $errors->first('codice') }}
        </span>
    @endif
</div>

<div class="form-group">
    <label for="indirizzo">Indirizzo</label>
    <input type="text" class="form-control" value="{{old('indirizzo', !empty($user->indirizzo) ? $user->indirizzo : '')}}" id="indirizzo" name="indirizzo">
    @if($errors->has('indirizzo'))
        <span class="text-danger" >
            {{ $errors->first('indirizzo') }}
        </span>
    @endif
</div>
<div class="form-group">
    <label for="citta">Citta</label>
    <input type="text" class="form-control" value="{{old('citta', !empty($user->citta) ? $user->citta : '')}}" id="citta" name="citta">
    @if($errors->has('citta'))
        <span class="text-danger" >
            {{ $errors->first('citta') }}
        </span>
    @endif
</div>
<div class="form-group">
    <label for="provincia">Provincia</label>
    <input type="text" class="form-control" value="{{old('provicia', !empty($user->provincia) ? $user->provincia : '')}}" id="provincia" name="provincia">
    @if($errors->has('provincia'))
        <span class="text-danger" >
            {{ $errors->first('provincia') }}
        </span>
    @endif
</div>
<div class="form-group">
    <label for="cap">Cap</label>
    <input type="text" class="form-control" id="cap" value="{{old('cap', !empty($user->cap) ? $user->cap : '')}}" name="cap">
    @if($errors->has('cap'))
        <span class="text-danger" >
            {{ $errors->first('cap') }}
        </span>
    @endif
</div>

<div class="form-group">
    <label for="cap">Telefono</label>
    <input type="text" class="form-control" id="telefono" value="{{old('telefono', !empty($user->telefono) ? $user->telefono : '')}}" name="cap">
    @if($errors->has('telefono'))
        <span class="text-danger" >
            {{ $errors->first('telefono') }}
        </span>
    @endif
</div>

<div class="form-group">
    <label for="cap">Cellulare</label>
    <input type="text" class="form-control" id="cellulare" value="{{old('cellulare', !empty($user->cellulare) ? $user->cellulare : '')}}" name="cap">
    @if($errors->has('cellulare'))
        <span class="text-danger" >
            {{ $errors->first('cellulare') }}
        </span>
    @endif
</div>

<div class="form-group">
    <label for="email">E-Mail*</label>
    <input type="text" class="form-control" id="email" value="{{old('email', !empty($user->email) ? $user->email : '')}}" name="email">
    @if($errors->has('email'))
        <span class="text-danger" >
            {{ $errors->first('email') }}
        </span>
    @endif
</div>
<div class="form-group">
    <label for="password">Password*</label>
    <input type="text" class="form-control" id="password" value="{{old('password')}}" name="password">
    @if($errors->has('password'))
        <span class="text-danger" >
            {{ $errors->first('password') }}
        </span>
    @endif
</div>
