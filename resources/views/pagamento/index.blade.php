@extends('layouts.dashboardAdmin')
@section('page_heading','Riepilogo acconti')
@section('section')

    @if (session('status'))
        <div class="alert  {!!  session('result') ? 'alert-success' : 'alert-warning' !!} ">
            {{ session('status') }}
        </div>
    @endif
    <div class="row" style="padding-top: 10px; padding-bottom: 10px;">
        <div class="col-sm-11 text-left">
            <form>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search" name="keywords">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-1 text-right">
            <a href="{{url('/admin/pagamento/create')}}" class="btn btn-success"><i class="fa fa-plus"></i></a>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Responsabile</th>
                <th>Importo</th>
                <th>Data</th>
                <th>Stato</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
                @forelse($pagamenti as $pagamento)
                    <tr>
                        <td>{{$pagamento->attachable->cognome}} {{$pagamento->attachable->nome}}</td>
                        <td>{{$pagamento->importo}}&euro;</td>
                        <td>{{$pagamento->data_pagamento->format("d/m/Y")}}</td>
                        <td>{{$pagamento->stato}}</td>
                        <td>
                            <button type="button" class="btn btn-info show-btn" data-pagamento-id="{{$pagamento->id}}"><i class="fa fa-info"></i></button>
                            <a href="{{url('/admin/pagamento/edit/'.$pagamento->id)}}" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="20" class="table-warning">Nessun Pagmento</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    {{ $pagamenti->links() }}

    <!-- Modal -->
    <div id="pagamentoShow" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Dettaglio Pagamento</h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <a href="" class="btn btn-warning" id="mod-pagamento"><i class="fa fa-pencil"></i> Modifica</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Chiudi</button>
                </div>
            </div>

        </div>
    </div>
@stop
@section('script')
    <script>

        $(".show-btn").click(function(e){
            $("#pagamentoShow").find(".modal-body").html("<div class='text-center'><i class=\"fa fa-spinner fa-spin fa-3x fa-fw\"></i></div>");
            var pagamentoId = $(this).data('pagamentoId');

            $("#pagamentoShow").modal();
            $.ajax({
                method: "POST",
                url: "{{url('admin/pagamento/ajax/show/')}}",
                data: { _token:'{{csrf_token()}}',id:pagamentoId}
            })
            .done(function( msg ) {
                $("#pagamentoShow").find(".modal-body").html(msg);
                $("#mod-pagamento").attr('href','{{url('/admin/pagamento/edit/')}}/'+pagamentoId);
            });

        });

    </script>
@stop
