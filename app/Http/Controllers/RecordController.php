<?php

namespace App\Http\Controllers;

use App\Http\Requests\RecordRequest;
use App\Imports\RecordsImport;
use App\Record;
use Illuminate\Http\Request;
use Excel;

class RecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('record.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $record = new Record();

        $record->sindacato = $request->sindacato ;
        $record->federazione = $request->federazione ;
        $record->codice_sede = $request->codice_sede ;
        $record->sede_in_chiaro = $request->sede_in_chiaro ;
        $record->tipo_presentazione = $request->tipo_presentazione ;
        $record->nome_cognome = $request->nome_cognome ;
        $record->codice_fiscale = $request->codice_fiscale ;
        $record->data_nascita = $request->data_nascita ;
        $record->importo_trattenuta = $request->importo_trattenuta ;
        $record->data_valuta = $request->data_valuta ;
        $record->data_pagamento = $request->data_pagamento ;
        $record->numero_domande = $request->numero_domande ;
        $record->comune_residenza = $request->comune_residenza ;
        $record->codice_patronato = $request->codice_patronato ;
        $record->ufficio_patronato = $request->ufficio_patronato ;
        $record->patronato = $request->patronato ;
        $record->save();

        return redirect('/admin/records');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $record = Record::find($id);
        return $record;
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ajaxShow(Request $request)
    {
        $id = $request->id;
        $record = Record::find($id);

        return view('record.ajax.show',compact('record'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Record $record)
    {
        return view('record.edit',compact('record'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Record $record)
    {
        //
        $record->sindacato = $request->sindacato ;
        $record->federazione = $request->federazione ;
        $record->codice_sede = $request->codice_sede ;
        $record->sede_in_chiaro = $request->sede_in_chiaro ;
        $record->tipo_presentazione = $request->tipo_presentazione ;
        $record->nome_cognome = $request->nome_cognome ;
        $record->codice_fiscale = $request->codice_fiscale ;
        $record->data_nascita = $request->data_nascita ;
        $record->importo_trattenuta = $request->importo_trattenuta ;
        $record->data_valuta = $request->data_valuta ;
        $record->data_pagamento = $request->data_pagamento ;
        $record->numero_domande = $request->numero_domande ;
        $record->comune_residenza = $request->comune_residenza ;
        $record->codice_patronato = $request->codice_patronato ;
        $record->ufficio_patronato = $request->ufficio_patronato ;
        $record->patronato = $request->patronato ;
        $record->save();

        return redirect('/admin/record/edit/'.$record->id)->with('status','Modifiche effettuate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function readXls(Request $request){

        try{
            Excel::import(new RecordsImport, $request->file('file'));
            return redirect('/admin/records')->with('status','File caricato correttamente')->with('result',true);
        }
        catch (\Exception $e){
            return redirect('/admin/records')->with('status','Errore file controllare che le colonne!')->with('result',false);
        }

    }

}
