@extends('layouts.dashboardAdmin')
@section('page_heading','Records')
@section('section')

    @if (session('status'))
        <div class="alert  {!!  session('result') ? 'alert-success' : 'alert-warning' !!} ">
            {{ session('status') }}
        </div>
    @endif

    <div class="text-right" style="padding-top: 10px; padding-bottom: 10px;">
        <form action="{{url('/admin/record/import/xls')}}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="file" name="file" class="form-control">
            <button class="btn btn-warning">Carica</button>

        </form>

    </div>

    <div class="row" style="padding-top: 10px; padding-bottom: 10px;">
        <div class="col-sm-11 text-left">
            <form>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search" name="keywords">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-1 text-right">
            <a href="{{url('/admin/record/create')}}" class="btn btn-success"><i class="fa fa-plus"></i></a>
        </div>
    </div>


    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Sindacato</th>
                <th>Federazione</th>
                <th>Codice Sede</th>
                <th>Sede in chiaro</th>
                <th>Tipo presentazione</th>
                <th>Nome Cognome</th>
                <th>Codice Fiscale</th>
                <th>Data nascita</th>
                <th>Importo trattenuta</th>
                <th>Data valuta</th>
                <th>Data pagamento</th>
                <th>Numero domande</th>
                <th>Comune residenza</th>
                <th>Codice patronato</th>
                <th>Ufficio patronato</th>
                <th>Patronato</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
                @forelse($records as $record)
                    <tr>
                        <td>{{$record->sindacato}}</td>
                        <td>{{$record->federazione}}</td>
                        <td>
                            {{$record->codice_sede}}
                        </td>
                        <td>
                            {{$record->sede_in_chiaro}}
                        </td>
                        <td>
                            {{$record->tipo_presentazione}}
                        </td>
                        <td>
                            {{$record->nome_cognome}}
                        </td>
                        <td>
                            {{$record->codice_fiscale}}
                        </td>
                        <td>
                            @if($record->data_nascita)
                            {{$record->data_nascita->format("d/m/Y")}}
                            @endif
                        </td>
                        <td>
                            {{$record->importo_trattenuta}}
                        </td>
                        <td>
                            @if($record->data_valuta)
                            {{$record->data_valuta->format("d/m/Y")}}
                            @endif
                        </td>
                        <td>
                            @if($record->data_valuta)
                            {{$record->data_pagamento->format("d/m/Y")}}
                            @endif
                        </td>
                        <td>
                            {{$record->numero_domande}}
                        </td>
                        <td>
                            {{$record->comune_residenza}}
                        </td>
                        <td>
                            {{$record->codice_patronato}}
                        </td>
                        <td>
                            {{$record->ufficio_patronato}}
                        </td>
                        <td>
                            {{$record->patronato}}
                        </td>
                        <td>
                            <button type="button" class="btn btn-info show-btn" data-record-id="{{$record->id}}"><i class="fa fa-info"></i></button>
                            <a href="{{url('/admin/record/edit/'.$record->id)}}" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="20" class="table-warning">Nessun record</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    {{ $records->links() }}

    <!-- Modal -->
    <div id="recordShow" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Dettaglio Record</h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <a href="" class="btn btn-warning" id="mod-record"><i class="fa fa-pencil"></i> Modifica</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Chiudi</button>
                </div>
            </div>

        </div>
    </div>
@stop
@section('script')
    <script>

        $(".show-btn").click(function(e){
            $("#recordShow").find(".modal-body").html("<div class='text-center'><i class=\"fa fa-spinner fa-spin fa-3x fa-fw\"></i></div>");
            var recordId = $(this).data('recordId');

            $("#recordShow").modal();
            $.ajax({
                method: "POST",
                url: "{{url('admin/record/ajax/show')}}",
                data: { _token:'{{csrf_token()}}',id: recordId }
            })
            .done(function( msg ) {
                $("#recordShow").find(".modal-body").html(msg);
                $("#mod-record").attr('href','{{url('/admin/record/edit/')}}/'+recordId);
            });

        });

    </script>
@stop
