<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SedeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'denominazione' => 'required|string|max:50',
            'codice' => 'required|string|max:50',
            'provincia' => 'nullable|regex:/^[A-Za-z]{2}$/i',
            'responsabile_quota' => 'nullable|numeric',
            'referente_quota' =>'nullable|numeric',
        ];
    }

    public function messages()
    {
        return [
            'denominazione.required' =>'Campo obbligatorio',
            'codice.required' =>'Campo obbligatorio',
            'provincia.regex' => 'Formato non valido',
            'responsabile_quota.numeric' => 'Campo numerico',
            'referente_quota.numeric' => 'Campo numerico',
        ];
    }
}
