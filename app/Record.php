<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    //
    protected $fillable = [ 'sindacato',
                            'federazione',
                            'codice_sede',
                            'sede_in_chiaro',
                            'tipo_presentazione',
                            'nome_cognome',
                            'codice_fiscale',
                            'data_nascita',
                            'importo_trattenuta',
                            'data_valuta',
                            'data_pagamento',
                            'numero_domande',
                            'comune_residenza',
                            'codice_patronato',
                            'ufficio_patronato',
                            'patronato'];

    protected $dates = ['data_nascita','data_valuta','data_pagamento'];

    public function users(){
        return $this->hasMany(Addetto::class,'ufficio_patronato','record_id');
    }

    public function getNomeAttribute(){
        $array = explode("/",$this->nome_cognome);
        return $array[1];
    }

    public function getCognomeAttribute(){
        $array = explode("/",$this->nome_cognome);
        return $array[0];
    }

    public function liquidato()
    {
        return $this->morphMany(LiquidatoBody::class, 'attachable');
    }

}
