<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\UserCategory;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = UserCategory::all();
        return view('user.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {

        $user = new User();
        $user->category_id = $request->category_id;
        $user->nome = $request->nome;
        $user->cognome = $request->cognome;
        $user->data_di_nascita = $request->data_di_nascita;
        $user->codice_fiscale = $request->codice_fiscale;
        $user->codice = $request->codice;
        $user->indirizzo = $request->indirizzo;
        $user->citta = $request->citta;
        $user->provincia = $request->provincia;
        $user->cap = $request->cap;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();

        return redirect('admin/users');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ajaxShow(Request $request)
    {
        $user_id = $request->id;
        $user = User::find($user_id);
        return view('user.ajax.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = UserCategory::all();
        $user = User::find($id);
        return view('user.edit',compact('user','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $user = User::find($id);
        $user->category_id = $request->category_id;
        $user->nome = $request->nome;
        $user->cognome = $request->cognome;
        $user->data_di_nascita = $request->data_di_nascita;
        $user->codice_fiscale = $request->codice_fiscale;
        $user->codice = $request->codice;
        $user->indirizzo = $request->indirizzo;
        $user->citta = $request->citta;
        $user->provincia = $request->provincia;
        $user->cap = $request->cap;
        $user->email = $request->email;
        if(!empty($request->password)){
            $user->password = Hash::make($request->password);
        }
        $user->save();
        return redirect('admin/user/edit/'.$id)->with('status','Utente modificato!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
