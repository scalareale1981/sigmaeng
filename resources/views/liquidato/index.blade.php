@extends('layouts.dashboardAdmin')
@section('page_heading','Riepilogo liquidazioni')
@section('section')

    @if (session('status'))
        <div class="alert  {!!  session('result') ? 'alert-success' : 'alert-warning' !!} ">
            {{ session('status') }}
        </div>
    @endif

    <div class="row" style="padding-top: 10px; padding-bottom: 10px;">
        <div class="col-sm-11 text-left">
            <form>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search" name="keywords">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-1 text-right">
            <a href="{{url('/admin/liquidazione/create')}}" class="btn btn-success"><i class="fa fa-plus"></i></a>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Data</th>
                <th>Responsabile</th>
                <th>Importo</th>
                <th>Liquidati</th>
                <th>Elaborati</th>
                <th>Acconti</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @forelse($liquidazioni as $liquidazione)
                <tr>
                    <td>{{$liquidazione->data_liquidazione->format("d/m/Y")}}</td>
                    <td>{{$liquidazione->user->cognome}} {{$liquidazione->user->nome}}</td>
                    <td>{{$liquidazione->importo}}&euro;</td>
                    <td>{{$liquidazione->liquidati->count()}}</td>
                    <td>{{$liquidazione->liquidati->count() + $liquidazione->elaborati->count()}}</td>
                    <td>{{$liquidazione->acconti->count()}}</td>
                    <td class="text-center">
                        <button type="button" class="btn btn-info show-btn" data-liquidazione-id="{{$liquidazione->id}}"><i class="fa fa-info"></i></button>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="20" class="table-warning">Nessun Pagmento</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
    {{ $liquidazioni->links() }}

    <!-- Modal -->
    <div id="liquidazioneShow" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Dettaglio Saldo</h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <a href="" class="btn btn-warning" id="mod-liquidazione"><i class="fa fa-pencil"></i> Modifica</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Chiudi</button>
                </div>
            </div>

        </div>
    </div>
@stop
@section('script')
    <script>

        $(".show-btn").click(function(e){
            $("#liquidazioneShow").find(".modal-body").html("<div class='text-center'><i class=\"fa fa-spinner fa-spin fa-3x fa-fw\"></i></div>");
            var liquidazioneId = $(this).data('liquidazioneId');

            $("#liquidazioneShow").modal();
            $.ajax({
                method: "POST",
                url: "{{url('admin/liquidazione/ajax/show/')}}",
                data: { _token:'{{csrf_token()}}',id:liquidazioneId}
            })
                .done(function( msg ) {
                    $("#liquidazioneShow").find(".modal-body").html(msg);
                    $("#mod-liquidazione").attr('href','{{url('/admin/liquidazione/edit/')}}/'+liquidazioneId);
                });

        });

    </script>
@stop
