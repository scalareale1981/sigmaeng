@extends('layouts.dashboardAdmin')
@section('page_heading','Modifica record')
@section('section')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <div class="col-12 form-area" id="form">
        <form action="{{url('/admin/record/update/'.$record->id)}}" method="post">
            @csrf
            @method('PATCH')
            @include('widgets.formRecord',['record'=>$record])
            <button type="submit" class="btn btn-primary">Salva</button>
        </form>
    </div>
    <div style="margin:10px;">&nbsp;</div>
@stop
