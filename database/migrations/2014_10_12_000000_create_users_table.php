<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;
use Illuminate\Support\Facades\Hash;


class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id');
            $table->integer('tipo_sede')->nullable();
            $table->string('nome')->nullable();
            $table->string('cognome')->nullable();
            $table->string('denominazione')->nullable();
            $table->date('data_di_nascita')->nullable();
            $table->string('codice_fiscale')->nullable();
            $table->string('codice')->nullable();
            $table->string('indirizzo')->nullable();
            $table->string('citta')->nullable();
            $table->string('provincia')->nullable();
            $table->string('cap')->nullable();
            $table->string('email')->unique();
            $table->string('telefono')->nullable();
            $table->string('cellulare')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();

            //$table->foreign('referente')->references('referente')->on('addetti');
        });

        $user = new User();
        $user->nome = 'admin';
        $user->cognome = 'admin';
        $user->category_id = 1;
        $user->email = 'admin@admin.it';
        $user->password = Hash::make('2018');
        $user->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
