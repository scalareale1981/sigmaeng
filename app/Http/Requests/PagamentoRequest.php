<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PagamentoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required',
            'id' => 'required',
            'importo' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'data_pagamento' => 'required|date_format:Y-m-d',
            'nota' => 'max:512'
        ];
    }

    public function messages()
    {
        return [
            'type.required' =>'Campo obbligatorio',
            'codice.required' =>'Campo obbligatorio',
            'importo.required' => 'Campo obbligatorio',
            'importo.regex' => 'Campo numerico con 2 decimali',
            'data_pagamento.required' => 'Campo obbligatorio',
            'data_pagamento.date_format' => 'Formato data non valido',
            'nota.max' => 'Superato il massimo numero di caratteri',
        ];
    }
}
