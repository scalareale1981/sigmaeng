<input type="hidden" name="category_id" value="1">
<div class="form-group">
    <label for="nome">Nome*</label>
    <input type="text" class="form-control" value="{{old('nome', !empty($user->nome) ? $user->nome : '')}}" name="nome" id="nome">
    @if($errors->has('nome'))
        <span class="text-danger" >
            {{ $errors->first('nome') }}
        </span>
    @endif
</div>
<div class="form-group">
    <label for="cognome">Cognome*</label>
    <input type="text" class="form-control" value="{{old('cognome', !empty($user->cognome) ? $user->cognome : '')}}" name="cognome" id="cognome">
    @if($errors->has('cognome'))
        <span class="text-danger" >
            {{ $errors->first('cognome') }}
        </span>
    @endif
</div>
<div class="form-group">
    <label for="citta">Citta</label>
    <input type="text" class="form-control" value="{{old('citta', !empty($user->citta) ? $user->citta : '')}}" id="citta" name="citta">
    @if($errors->has('citta'))
        <span class="text-danger" >
            {{ $errors->first('citta') }}
        </span>
    @endif
</div>
<div class="form-group">
    <label for="provincia">Provincia</label>
    <input type="text" class="form-control" value="{{old('provincia', !empty($user->provincia) ? $user->provincia : '')}}" id="provincia" name="provincia">
    @if($errors->has('provincia'))
        <span class="text-danger" >
            {{ $errors->first('provincia') }}
        </span>
    @endif
</div>
<div class="form-group">
    <label for="cap">Cap</label>
    <input type="text" class="form-control" id="cap" value="{{old('cap', isset($user->cap) ? $user->cap : '')}}" name="cap">
    @if($errors->has('cap'))
        <span class="text-danger" >
            {{ $errors->first('cap') }}
        </span>
    @endif
</div>
<div class="form-group">
    <label for="email">E-Mail*</label>
    <input type="text" class="form-control" id="email" value="{{old('email', isset($user->email) ? $user->email : '')}}" name="email">
    @if($errors->has('email'))
        <span class="text-danger" >
            {{ $errors->first('email') }}
        </span>
    @endif
</div>
<div class="form-group">
    <label for="password">Password*</label>
    <input type="text" class="form-control" id="password" value="{{old('password')}}" name="password">
    @if($errors->has('password'))
        <span class="text-danger" >
            {{ $errors->first('password') }}
        </span>
    @endif
</div>
