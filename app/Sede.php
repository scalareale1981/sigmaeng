<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sede extends Model
{
    //
    protected $table = 'sedi';

    public function responsabile(){
        return $this->belongsTo(User::class,'responsabile_id');
    }

    public function referente(){
        return $this->belongsTo(User::class,'referente_id');
    }

    public function tipo_sede(){
        return $this->belongsTo(TipoSede::class,'tipo_sede_id');
    }

    public function records(){
        return $this->hasMany(Record::class,'ufficio_patronato','codice');
    }

    public function records_attivi(){
        return $this->hasMany(Record::class,'ufficio_patronato','codice')->where('pagato','=',0);
    }
}
