<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $dates = array('data_di_nascita','created_at','updated_at');

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome','cognome','nome','codice_fiscale','codice', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function category(){
        return $this->belongsTo(UserCategory::class,'category_id');
    }

    public function isAdmin(){
        if($this->category_id == 1){
            return true;
        }
        return false;
    }

    public function isAddetto(){
        if($this->category_id == 3){
            return true;
        }
        return false;
    }


    public function responsabile(){
        return $this->hasMany(Sede::class,'responsabile_id');
    }

    public function referente(){
        return $this->hasMany(Sede::class,'referente_id');
    }

    public function pagamenti()
    {
        return $this->morphMany(Pagamento::class, 'attachable');
    }

    public function pagamenti_attivi()
    {
        return $this->morphMany(Pagamento::class, 'attachable')->where('scalato','=',0);
    }

}
