<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Liquidato extends Model
{
    protected $table = 'liquidati';
    protected $dates = ['data_liquidazione','created_at','updated_at'];

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function body(){
        return $this->hasMany(LiquidatoBody::class,'liquidato_id');
    }

    public function liquidati(){
        return $this->hasMany(LiquidatoBody::class,'liquidato_id')->where('tipo','like','pagato');
    }
    public function elaborati(){
        return $this->hasMany(LiquidatoBody::class,'liquidato_id')->where('tipo','like','elaborato');
    }

    public function acconti(){
        return $this->hasMany(LiquidatoBody::class,'liquidato_id')->where('tipo','like','acconto');
    }

}
