<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSediTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sedi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('denominazione');
            $table->string('indirizzo')->nullable();
            $table->string('citta')->nullable();
            $table->string('provincia')->nullable();
            $table->string('codice')->nullable();
            $table->integer('tipo_sede_id');
            $table->integer('responsabile_id')->nullable();
            $table->double('responsabile_quota')->nullable();
            $table->integer('referente_id')->nullable();
            $table->double('referente_quota')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sedi');
    }
}
