<input type="hidden" name="type" value="user">
<div class="form-group">
    <label for="id">responsabile</label>
    <select name="id" class="form-control" id="id">
        <option value="">--</option>
        @foreach($responsabili as $responsabile)
            <option value="{{$responsabile->id}}" {{old('id',isset($pagamento->attachable_id) ? $pagamento->attachable_id : '') == $responsabile->id ? 'selected' : ''}}>{{$responsabile->cognome}} {{$responsabile->nome}}</option>
        @endforeach
    </select>
    @if($errors->has('id'))
        <span class="text-danger" >
            {{ $errors->first('id') }}
        </span>
    @endif
</div>

<div class="form-group">
    <label for="importo">Importo</label>
    <input type="number" class="form-control" id="importo" value="{{old('importo',isset($pagamento->importo) ? $pagamento->importo : '')}}" name="importo" step="0.01">
    @if($errors->has('importo'))
        <span class="text-danger" >
            {{ $errors->first('importo') }}
        </span>
    @endif
</div>

<div class="form-group">
    <label for="data_pagamento">Data pagamento</label>
    <input type="date" class="form-control" id="data_pagamento" value="{{old('data_pagamento',isset($pagamento->data_pagamento) ? $pagamento->data_pagamento->format("Y-m-d") : date("Y-m-d"))}}" name="data_pagamento">
    @if($errors->has('data_pagamento'))
        <span class="text-danger" >
            {{ $errors->first('data_pagamento') }}
        </span>
    @endif
</div>

<div class="form-group">
    <label for="nota">Nota</label>
    <textarea class="form-control" id="nota"  name="nota">{{old('nota',isset($pagamento->nota) ? $pagamento->nota : '')}}</textarea>
    @if($errors->has('nota'))
        <span class="text-danger" >
            {{ $errors->first('nota') }}
        </span>
    @endif
</div>
