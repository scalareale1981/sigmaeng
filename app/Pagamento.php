<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pagamento extends Model
{
    protected $table = 'pagamenti';
    protected $dates = ['data_pagamento','deleted_at'];



    public function attachable()
    {
        return $this->morphTo();
    }


    public function liquidato()
    {
        return $this->morphMany(LiquidatoBody::class, 'attachable');
    }

    public function getStatoAttribute(){
        if($this->scalato){
            return 'Utilizzato';
        }
        else{
            return 'Non utilizzato';
        }
    }
}
