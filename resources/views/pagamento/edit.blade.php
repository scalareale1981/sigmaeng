@extends('layouts.dashboardAdmin')
@section('page_heading','Modifica acconto')
@section('section')

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <div class="col-12 form-area" id="form">
        <form action="{{url('/admin/pagamento/update/'.$pagamento->id)}}" method="post">
            @csrf
            @method('PATCH')
            @include('widgets.formPagamento',['pagamento'=>$pagamento])
            <button type="submit" class="btn btn-primary">Salva</button>
        </form>
    </div>
    <div style="margin:10px;">&nbsp;</div>
@stop
