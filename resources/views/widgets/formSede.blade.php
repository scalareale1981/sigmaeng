<input type="hidden" name="category_id" value="3">
<div class="form-group">
    <label for="denominazione">Denominazione</label>
    <input type="text" class="form-control" value="{{old('denominazione',isset($sede->denominazione) ? $sede->denominazione : '' )}}"  id="denominazione" name="denominazione">
    @if($errors->has('denominazione'))
        <span class="text-danger" >
            {{ $errors->first('denominazione') }}
        </span>
    @endif
</div>
<div class="form-group">
    <label for="codice">Codice</label>
    <input type="text" class="form-control" value="{{old('codice',isset($sede->codice) ? $sede->codice : '' )}}" id="codice" name="codice">
    @if($errors->has('codice'))
        <span class="text-danger" >
            {{ $errors->first('codice') }}
        </span>
    @endif
</div>
<div class="form-group">
    <label for="tipo_sede_id">Tipo sede</label>
    <select class="form-control" id="tipo_sede_id"  name="tipo_sede_id">
        @foreach($tipo_sedi as $tipo)
            <option value="{{$tipo->id}}" {{(old('tipo_sede_id',isset($sede->tipo_sede_id) ? $sede->tipo_sede_id : '' )) == $tipo->id ? 'selected' : ''}}>{{$tipo->descrizione}}</option>
        @endforeach
    </select>

    @if($errors->has('indirizzo'))
        <span class="text-danger" >
            {{ $errors->first('indirizzo') }}
        </span>
    @endif
</div>
<div class="form-group">
    <label for="citta">Indirizzo</label>
    <input type="text" class="form-control" value="{{old('indirizzo',isset($sede->indirizzo) ? $sede->indirizzo : '')}}" id="indirizzo" name="indirizzo">
    @if($errors->has('indirizzo'))
        <span class="text-danger" >
            {{ $errors->first('indirizzo') }}
        </span>
    @endif
</div>
<div class="form-group">
    <label for="citta">Citta</label>
    <input type="text" class="form-control" value="{{old('citta',isset($sede->citta) ? $sede->citta : '')}}" id="citta" name="citta">
    @if($errors->has('citta'))
        <span class="text-danger" >
            {{ $errors->first('citta') }}
        </span>
    @endif
</div>
<div class="form-group">
    <label for="provincia">Provincia</label>
    <input type="text" class="form-control" value="{{old('provincia',isset($sede->provincia) ? $sede->provincia : '')}}" id="provincia" name="provincia">
    @if($errors->has('provincia'))
        <span class="text-danger" >
            {{ $errors->first('provincia') }}
        </span>
    @endif
</div>
<div class="form-group">
    <label for="cap">Cap</label>
    <input type="text" class="form-control" id="cap" value="{{old('cap',isset($sede->cap) ? $sede->cap : '')}}" name="cap">
    @if($errors->has('cap'))
        <span class="text-danger" >
            {{ $errors->first('cap') }}
        </span>
    @endif
</div>
<div class="form-group">
    <label for="responsabile_id">Responsabile</label>
    <select class="form-control" id="responsabile_id" name="responsabile_id">
        <option value="0">NON DEFINITO</option>
        @foreach($users as $user)
            <option value="{{$user->id}}" {{(old('responsabile_id',isset($sede->responsabile_id) ? $sede->responsabile_id : '' )) == $user->id ? 'selected' : ''}}>{{$user->nome}} {{$user->cognome}}</option>
        @endforeach
    </select>
    @if($errors->has('responsabile_id'))
        <span class="text-danger" >
            {{ $errors->first('responsabile_id') }}
        </span>
    @endif
</div>

<div class="form-group">
    <label for="responsabile_quota">Responsabile Quota</label>
    <input type="number" class="form-control" id="responsabile_quota" value="{{old('responsabile_quota',isset($sede->responsabile_quota) ? $sede->responsabile_quota : '')}}" name="responsabile_quota">
    @if($errors->has('responsabile_quota'))
        <span class="text-danger" >
            {{ $errors->first('responsabile_quota') }}
        </span>
    @endif
</div>

<div class="form-group">
    <label for="referente_id">Referente</label>
    <select class="form-control" id="referente_id" value="{{old('referente_id')}}" name="referente_id">
        <option value="0">NON DEFINITO</option>
        @foreach($users as $user)
            <option value="{{$user->id}}" {{(old('referente_id',isset($sede->referente_id) ? $sede->referente_id : '' )) == $user->id ? 'selected' : ''}}>{{$user->nome}} {{$user->cognome}}</option>
        @endforeach
    </select>
    @if($errors->has('referente_id'))
        <span class="text-danger" >
            {{ $errors->first('referente_id') }}
        </span>
    @endif
</div>

<div class="form-group">
    <label for="referente_quota">Referente Quota</label>
    <input type="number" class="form-control" id="referente_quota" value="{{old('referente_quota',isset($sede->referente_quota) ? $sede->referente_quota : '')}}" name="referente_quota">
    @if($errors->has('referente_quota'))
        <span class="text-danger" >
            {{ $errors->first('referente_quota') }}
        </span>
    @endif
</div>
