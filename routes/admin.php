<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home','AdminController@dashboard');

Route::get('/users','AdminController@users');

Route::get('/sedi','AdminController@sedi');

Route::get('/records','AdminController@records');

Route::get('/pagamenti','AdminController@pagamenti');

Route::get('/liquidazioni','AdminController@liquidazioni');

Route::get('/user/index','UserController@index');
Route::get('/user/create','UserController@create');
Route::post('/user/store','UserController@store');
Route::get('/user/edit/{id}','UserController@edit');
Route::patch('/user/update/{id}','UserController@update');
Route::post('/user/show','UserController@show');
Route::post('/user/ajax/show','UserController@ajaxShow');



Route::get('/sede/index','SedeController@index');
Route::get('/sede/create','SedeController@create');
Route::post('/sede/store','SedeController@store');
Route::get('/sede/edit/{sede}','SedeController@edit');
Route::patch('/sede/update/{sede}','SedeController@update');
Route::post('/sede/ajax/show','SedeController@ajaxShow');


Route::get('/record/index','RecordController@index');
Route::get('/record/create','RecordController@create');
Route::post('/record/store','RecordController@store');
Route::get('/record/edit/{record}','RecordController@edit');
Route::patch('/record/update/{record}','RecordController@update');
Route::post('/record/ajax/show','RecordController@ajaxShow');
Route::post('/record/import/xls','RecordController@readXls');

Route::get('/pagamento/index','PagamentoController@index');
Route::get('/pagamento/create','PagamentoController@create');
Route::post('/pagamento/store','PagamentoController@store');
Route::get('/pagamento/edit/{pagamento}','PagamentoController@edit');
Route::patch('/pagamento/update/{pagamento}','PagamentoController@update');
Route::post('/pagamento/ajax/show','PagamentoController@ajaxShow');


Route::get('/ajax/riepilogo/quote','AdminController@riepilogoQuote');



Route::get('/liquidazione/index','LiquidatoController@index');
Route::get('/liquidazione/create','LiquidatoController@create');
Route::post('/liquidazione/store','LiquidatoController@store');
Route::get('/liquidazione/edit/{record}','LiquidatoController@edit');
Route::patch('/liquidazione/update/{record}','LiquidatoController@update');
Route::post('/liquidazione/ajax/show','LiquidatoController@ajaxShow');

Route::post('/liquidazione/calcola','LiquidatoController@calcola');





