@extends('layouts.dashboardAdmin')
@section('page_heading','Crea nuovo Record')
@section('section')
    <div class="col-12 form-area" id="form">
        <form action="{{url('/admin/record/store')}}" method="post">
            @csrf
            @include('widgets.formRecord')
            <button type="submit" class="btn btn-primary">Salva</button>
        </form>
    </div>
    <div style="margin:10px;">&nbsp;</div>
@stop
