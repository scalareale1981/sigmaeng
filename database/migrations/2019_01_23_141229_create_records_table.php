<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('records', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sindacato')->nullable();
            $table->string('federazione')->nullable();
            $table->string('codice_sede')->nullable();
            $table->string('sede_in_chiaro')->nullable();
            $table->string('tipo_presentazione')->nullable();
            $table->string('nome_cognome')->nullable();
            $table->string('codice_fiscale')->nullable();
            $table->date('data_nascita')->nullable();
            $table->double('importo_trattenuta')->nullable();
            $table->date('data_valuta')->nullable();
            $table->date('data_pagamento')->nullable();
            $table->integer('numero_domande')->nullable();
            $table->string('comune_residenza')->nullable();
            $table->string('codice_patronato')->nullable();
            $table->string('ufficio_patronato')->nullable();
            $table->integer('pagato')->default(0);
            $table->integer('elaborato')->default(0);
            $table->string('patronato')->nullable();
            $table->timestamps();

            //$table->foreign('ufficio_patronato')->references('record_id')->on('addetti');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('records');
    }
}
