<?php

namespace App\Imports;

use App\Record;
use Maatwebsite\Excel\Concerns\ToModel;

class RecordsImport implements ToModel
{

    public function transformDate($value, $format = 'Y-m-d')
    {
        try {
            return \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
        } catch (\ErrorException $e) {
            return \Carbon\Carbon::createFromFormat($format, $value);
        }
    }

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {



        return new Record([
            'sindacato' => isset($row[0]) ? $row[0] : '',
            'federazione' => isset($row[1]) ? $row[1] : '',
            'codice_sede' => isset($row[2]) ? $row[2] : '',
            'sede_in_chiaro' => isset($row[3]) ? $row[3] : '',
            'tipo_presentazione' => isset($row[4]) ? $row[4] : '',
            'nome_cognome' => isset($row[5]) ? $row[5] : '',
            'codice_fiscale' => isset($row[6]) ? $row[6] : '',
            'data_nascita' =>  isset($row[7]) ? $this->transformDate($row[7]) : null,
            'importo_trattenuta' => (isset($row[8]) && (int)$row[8]>0) ? str_replace(".",",",$row[8]) : 0,
            'data_valuta' =>  isset($row[9]) ?  $this->transformDate($row[9]) : null,
            'data_pagamento' =>  isset($row[10]) ? $this->transformDate($row[10]) : null,
            'numero_domande' =>  (isset($row[11]) && (int)$row[11]>0) ? (int)$row[11] : 0,
            'comune_residenza' =>isset($row[12]) ? $row[12] : '',
            'codice_patronato' => isset($row[13]) ? $row[13] : '',
            'ufficio_patronato' =>  isset($row[14]) ? $row[14] : '',
            'patronato' =>   isset($row[15]) ? $row[15] : '',
        ]);

//        return new Record([
//            'sindacato' => isset($row[0]) ? $row[0] : '',
//            'federazione' => isset($row[1]) ? $row[1] : '',
//            'codice_sede' =>  isset($row[2]) ? $row[2] : '',
//            'sede_in_chiaro' =>  isset($row[3]) ? $row[3] : '',
//            'tipo_presentazione' =>  isset($row[4]) ? $row[4] : '',
//            'nome_cognome' =>  isset($row[5]) ? $row[5] : '',
//            'codice_fiscale' =>  isset($row[6]) ? $row[6] : '',
//            'data_nascita' =>  '00-00-0000',
//            'importo_trattenuta' => '0',
//            'data_valuta' =>  '00-00-0000',
//            'data_pagamento' =>  '00-00-0000',
//            'numero_domande' =>  isset($row[11]) ? $row[11] : '',
//            'comune_residenza' =>  isset($row[12]) ? $row[12] : '',
//            'codice_patronato' =>  isset($row[13]) ? $row[13] : '',
//            'ufficio_patronato' =>  isset($row[14]) ? $row[14] : '',
//            'patronato' =>  isset($row[15]) ? $row[15] : '',
//        ]);

    }
}
