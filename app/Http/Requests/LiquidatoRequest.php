<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LiquidatoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'user_id'=>'required|min:1',
            'data-liquidazione' => 'required|date_format:Y-m-d',
            'record-elabora' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'record-pagare' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'acconto' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'saldo' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'record' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'data-liquidazione.required' => 'Campo obbligatorio',
            'record-elabora.required' =>  'Campo obbligatorio',
            'record-pagare.required' =>  'Campo obbligatorio',
            'acconto.required' =>  'Campo obbligatorio',
            'saldo.required' => 'Campo obbligatorio',
            'data-liquidazione.format' => 'Formato data non valido',
            'record-elabora.regex' =>  'Formato valuta non valido',
            'record-pagare.regex' =>  'Formato valuta non valido',
            'acconto.regex' =>  'Formato valuta non valido',
            'saldo.regex' => 'Formato valuta non valido',
            'record.required' => 'Selezionare almeno un record',
            'user_id' => 'Selezionare un responsabile'
        ];
    }
}
