<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLiquidatiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('liquidati', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->date('data_liquidazione');
            $table->double('importo');
            $table->double('acconto');
            $table->double('saldo');
            $table->string('nota',512);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('liquidati');
    }
}
