<?php

namespace App\Http\Controllers;

use App\Http\Requests\LiquidatoRequest;
use App\Liquidato;
use App\LiquidatoBody;
use App\Pagamento;
use App\Record;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;

class LiquidatoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $responsabili = DB::table('sedi')
            ->join('users','sedi.responsabile_id','=','users.id')
            ->select('users.*')
            ->orderBy('users.cognome')
            ->orderBy('users.nome')
            ->groupBy('users.id')
            ->get();

        return view('liquidato.create',compact('responsabili'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LiquidatoRequest $request)
    {

        $liquidato = new Liquidato();
        $liquidato->user_id = $request->user_id;
        $liquidato->data_liquidazione = $request->input('data-liquidazione');
        $liquidato->importo = $request->input('record-pagare');
        $liquidato->acconto = $request->input('acconto');
        $liquidato->saldo = $request->input('saldo');
        $liquidato->nota = '';
        $liquidato->save();

        $recordSend = array();
        if(isset($request->record['pagare'])) {
            foreach ($request->record['pagare'] as $record_id) {
                $record = Record::find($record_id);
                $record->elaborato = 1;
                $record->pagato = 1;
                $record->save();
                $liquidatoBody = new LiquidatoBody();
                $liquidatoBody->liquidato_id = $liquidato->id;
                $liquidatoBody->tipo = 'pagato';
                $record->liquidato()->save($liquidatoBody);
                $recordSend[$record_id] = $record_id;
            }
        }

        if(isset($request->record['elaborare'])) {
            foreach ($request->record['elaborare'] as $record_id) {
                if (!isset($recordSend[$record_id])) {
                    $record = Record::find($record_id);
                    $record->elaborato = 1;
                    $record->save();
                    $liquidatoBody = new LiquidatoBody();
                    $liquidatoBody->liquidato_id = $liquidato->id;
                    $liquidatoBody->tipo = 'elaborato';
                    $record->liquidato()->save($liquidatoBody);
                }
            }
        }

        if(isset($request->pagamento)){
            foreach ($request->pagamento as $pagamento_id){
                $record = Pagamento::find($pagamento_id);
                $record->scalato = 1;
                $record->save();
                $liquidatoBody = new LiquidatoBody();
                $liquidatoBody->liquidato_id = $liquidato->id;
                $liquidatoBody->tipo = 'acconto';
                $record->liquidato()->save($liquidatoBody);
            }
        }

        return redirect('admin/liquidazioni');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Liquidato  $liquidato
     * @return \Illuminate\Http\Response
     */
    public function show(Liquidato $liquidato)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Liquidato  $liquidato
     * @return \Illuminate\Http\Response
     */
    public function edit(Liquidato $liquidato)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Liquidato  $liquidato
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Liquidato $liquidato)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Liquidato  $liquidato
     * @return \Illuminate\Http\Response
     */
    public function destroy(Liquidato $liquidato)
    {
        //
    }

    public function calcola(Request $request){
        $referente_id = $request->id;
        $user = User::find($referente_id);
        return view('liquidato.ajax.calcola',compact('user'));

    }

    public function ajaxShow(Request $request){
        $liquidato_id = $request->id;
        $liquidato = Liquidato::find($liquidato_id);
        dd($liquidato);
        //return view('liquidato.ajax.show',compact('liquidato'));
    }

}
