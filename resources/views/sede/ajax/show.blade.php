<table class="table table-bordered table-striped">
    <tr>
        <td>
            Denominazione
        </td>
        <td>
            {{$sede->denominazione}}
        </td>
    </tr>
    <tr>
        <td>
            Codice
        </td>
        <td>
            {{$sede->codice}}
        </td>
    </tr>
    <tr>
        <td>
            Indirizzo
        </td>
        <td>
            {{$sede->indirizzo}}<br>{{$sede->citta}}<br>{{$sede->provincia}} {{$sede->cap}}
        </td>
    </tr>
    <tr>
        <td>
            Tipo sede
        </td>
        <td>
            {{$sede->tipo_sede->descrizione}}
        </td>
    </tr>
    <tr>
        <td>
            Referente
        </td>
        <td>
            @if($sede->referente)
                {{$sede->referente->nome}} {{$sede->referente->cognome}} - {{$sede->referente->email}}
            @endif
        </td>
    </tr>
    <tr>
        <td>
            Quota Referente
        </td>
        <td>
            {{$sede->referente_quota}}&percnt;
        </td>
    </tr>
    <tr>
        <td>
            Responsabile
        </td>
        <td>
            @if($sede->responsabile)
                {{$sede->responsabile->nome}} {{$sede->responsabile->cognome}} - {{$sede->responsabile->email}}
            @endif
        </td>
    </tr>
    <tr>
        <td>
            Quota Responsabile
        </td>
        <td>
            {{$sede->responsabile_quota}}&percnt;
        </td>
    </tr>
</table>
