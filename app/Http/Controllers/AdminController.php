<?php

namespace App\Http\Controllers;

use App\Addetto;
use App\Liquidato;
use App\Record;
use App\Sede;
use App\User;
use Illuminate\Http\Request;
use App\Pagamento;

class AdminController extends Controller
{
    //
    public function dashboard(){
        return view('admin.dashboard');
    }

    public function users(Request $request){
        $users = User::paginate(20);
        return view('user.index',compact('users'));
    }

    public function sedi(Request $request){
        $sedi = Sede::paginate(20);
        return view('sede.index',compact('sedi'));
    }

    public function records(Request $request){
        $records = Record::paginate(20);
        return view('record.index',compact('records'));
    }

    public function pagamenti(Request $request){

        if(!isset($request->keywords)){
            $pagamenti = Pagamento::paginate(20);
        }
        else{
            $pagamenti = Pagamento::paginate(20);
        }
        return view('pagamento.index',compact('pagamenti'));
    }

    public function liquidazioni(Request $request){

        $liquidazioni = Liquidato::paginate(20);
        return view('liquidato.index',compact('liquidazioni'));
    }

    public function riepilogoQuote(){
        $sediTemp = Sede::all();
        $sedi['data'] = array();
        $i=0;
        foreach ($sediTemp as $sede){
            $sedi['data'][$i][0] = $sede->responsabile->cognome." ".$sede->responsabile->nome;
            $sedi['data'][$i][1] = $sede->referente->cognome." ".$sede->referente->nome;
            $sedi['data'][$i][2] = $sede->denominazione;
            $sedi['data'][$i][3] = $sede->codice;
            $sedi['data'][$i][4] = $sede->tipo_sede->descrizione;
            $sedi['data'][$i][5] = 0;
            foreach ($sede->records as $record){
                $sedi['data'][$i][5] += ($record->importo_trattenuta*$sede->responsabile_quota)/100;
            }
            $i++;
        }
        return $sedi;
    }
}
