<div class="row">
    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">Records</div>
            <div class="panel-body table-responsive" style="padding: 0px;">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>
                            Elabora
                        </th>
                        <th>
                            Paga
                        </th>
                        <th>

                        </th>
                        <th style="width: 100px;">
                            Importo trattenuta
                        </th>
                        <th style="width: 100px;">
                            Responsabile quota
                        </th>
                        <th style="width: 100px;">
                            Quota
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @php $count=0 @endphp
                    @foreach($user->responsabile as $responsabile)
                        @foreach($responsabile->records_attivi as $record)
                            <tr>
                                <td>
                                    <input type="checkbox" class="elabora-check" name="record[elaborare][]" data-quota="{!! ($record->importo_trattenuta*$responsabile->responsabile_quota)/100 !!}" value="{{$record->id}}" onchange="calcola_importi()" checked>
                                </td>
                                <td>
                                    <input type="checkbox" class="record-check quota-check" onchange="calcola_importi()" id="quota-{{$record->id}}" data-quota="{!! ($record->importo_trattenuta*$responsabile->responsabile_quota)/100 !!}" name="record[pagare][]" value="{{$record->id}}" checked>
                                </td>
                                <td>
                                    {{$record->nome}}
                                    {{$record->cognome}}<br>
                                    {{$record->codice_fiscale}}
                                </td>
                                <td>
                                    {{$record->importo_trattenuta}}&euro;
                                </td>
                                <td>
                                    {{$responsabile->responsabile_quota}}%
                                </td>
                                <td>
                                    {!! ($record->importo_trattenuta*$responsabile->responsabile_quota)/100 !!} &euro;
                                </td>
                            </tr>
                            @php $count++ @endphp
                        @endforeach
                    @endforeach
                    @if(!$count)
                        <tr>
                            <td colspan="6" class="warning">
                                Nessun record da liquidare
                            </td>
                        </tr>
                    @endif
                    </tbody>

                </table>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">Acconti</div>
            <div class="panel-body table-responsive" style="padding: 0px;">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>

                        </th>
                        <th>
                            Data
                        </th>
                        <th style="width: 100px;">
                            Importo
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($user->pagamenti_attivi as $pagamento)
                        <tr>
                            <td>
                                <input type="checkbox" data-quota="{{$pagamento->importo}}" onchange="calcola_importi()" class="pagamento-check quota-check" name="pagamento[]" value="{{$pagamento->id}}" checked>
                            </td>
                            <td>
                                {{$pagamento->data_pagamento->format("d/m/Y")}}
                            </td>
                            <td>
                                {{$pagamento->importo}} &euro;
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="3" class="warning">
                                Nessun acconto
                            </td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

