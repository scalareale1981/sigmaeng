@extends('layouts.dashboardAdmin')
@section('page_heading','Liquida Responsabile')
@section('head')
    <style>
        #container-result {

            width: 100%;
            border-radius: 0px;
        }
    </style>
@endsection
@section('section')



    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="col-12 form-area" id="form">
        <form action="{{url('/admin/liquidazione/store')}}" method="post" id="form-liquidazione">
            @csrf
            <input type="hidden" name="type" value="user">
            <div class="form-group">
                <label for="id">Responsabile</label>
                <select name="user_id" class="form-control" id="id_responsabile">
                    <option value="">--</option>
                    @foreach($responsabili as $responsabile)
                        <option value="{{$responsabile->id}}" {{old('id',isset($pagamento->attachable_id) ? $pagamento->attachable_id : '') == $responsabile->id ? 'selected' : ''}}>{{$responsabile->cognome}} {{$responsabile->nome}}</option>
                    @endforeach
                </select>
                @if($errors->has('id'))
                    <span class="text-danger" >
                        {{ $errors->first('id') }}
                    </span>
                @endif
            </div>
            <div id="container-body"></div>
            <div id="container-result" class="alert alert-success" style="display: none;">
                <div class="row">
                    <div class="col-sm-3 text-right">
                        <div class="input-group">
                            <span class="input-group-addon">Data liquidazione</span>
                            <input type="date" name="data-liquidazione" value="{{date('Y-m-d')}}" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-3 texxt-left"><span id="container-elabora"></span><br><span id="container-pagato"></span><br><span id="container-acconto"></span><br><span id="container-saldo"></span></div>
                    <div class="col-sm-6 text-right">
                        <button type="button" class="btn btn-success" id="btn-liquidazione" onclick="$('#form-liquidazione').submit()">Procedi alla liquidazione</button>
                    </div>
                </div>
            </div>
        </form>

    </div>
    <div style="margin:10px;">&nbsp;</div>
@stop
@section('script')
    <script>
        function calcola_importi(){
            var pagamento = 0;
            var elabora = 0;
            var recordImporti = 0;

            $(".pagamento-check:checked").each(function(){
                var total_amount_string = $(this).data('quota');
                var total_amount_int = +parseFloat(total_amount_string).toFixed(2);
                pagamento += total_amount_int;
            });

            $(".elabora-check").each(function(){
                if(!$(this).prop('checked')){
                    var recordId = $(this).val();
                    $("#quota-"+recordId).prop('checked',false);

                }
                else{
                    var total_amount_string = $(this).data('quota');
                    var total_amount_int = +parseFloat(total_amount_string).toFixed(2);
                    elabora += total_amount_int;
                }
            });

            $(".record-check:checked").each(function(){
                var total_amount_string = $(this).data('quota');
                var total_amount_int = +parseFloat(total_amount_string).toFixed(2);
                recordImporti += total_amount_int;
            });

            var saldo = recordImporti - pagamento;
            $("#container-elabora").html("Elaborati: "+elabora+' &euro; <input type="hidden"  name="record-elabora" value="'+elabora+'">');
            $("#container-pagato").html("Pagare: "+recordImporti+' &euro; <input type="hidden"  name="record-pagare" value="'+recordImporti+'">');
            $("#container-acconto").html("Acconto: "+pagamento+' &euro; <input type="hidden"  name="acconto" value="'+pagamento+'">');
            $("#container-saldo").html("<strong>Saldo: "+saldo+' &euro;</strong> <input type="hidden"  name="saldo" value="'+saldo+'">');

            if(saldo>0){
                $("#container-result").show();
            }
        }

        function get_records(responsabileId){
            $("#container-result").hide();

            if(responsabileId>0){
                $("#container-body").html("<div class='text-center'><i class=\"fa fa-spinner fa-spin fa-3x fa-fw\"></i></div>");
                $.ajax({
                    method: "POST",
                    url: "{{url('admin/liquidazione/calcola/')}}",
                    data: { _token:'{{csrf_token()}}',id:responsabileId}
                })
                .done(function( msg ) {
                    $("#container-body").html(msg);
                    $("#container-result").show();
                    calcola_importi();
                });
            }
        }

        $("#id_responsabile").change(function(){
            var responsabileId = $(this).val();
            get_records(responsabileId)
        });

        $(function () {
            var responsabileId = $("#id_responsabile").val();
            if(responsabileId>0) {
                get_records(responsabileId);
            }
        })

    </script>
@stop
