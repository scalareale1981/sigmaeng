<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\TipoSede;

class CreateTipoSediTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_sedi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descrizione');
            $table->timestamps();


        });

//        Schema::table('addetti', function (Blueprint $table) {
//            $table->foreign('tipo_sede_id')->references('id')->on('tipo_sedi');
//        });
        $tipoSedi = new TipoSede();
        $tipoSedi->descrizione = 'NON DEFINITA' ;
        $tipoSedi->save();
        $tipoSedi = new TipoSede();
        $tipoSedi->descrizione = 'NO PATRONATO' ;
        $tipoSedi->save();
        $tipoSedi = new TipoSede();
        $tipoSedi->descrizione = 'P' ;
        $tipoSedi->save();
        $tipoSedi = new TipoSede();
        $tipoSedi->descrizione = 'R' ;
        $tipoSedi->save();
        $tipoSedi = new TipoSede();
        $tipoSedi->descrizione = 'VECCHIA SEDE CHIUSA' ;
        $tipoSedi->save();
        $tipoSedi = new TipoSede();
        $tipoSedi->descrizione = 'Z' ;
        $tipoSedi->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_sedi');
    }
}
