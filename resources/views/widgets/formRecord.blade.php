<div class="form-group">
    <label for="referente_quota">Sindacato</label>
    <input type="text" class="form-control" id="sindacato" value="{{old('sindacato',isset($record->sindacato) ? $record->sindacato : '')}}" name="sindacato">
    @if($errors->has('sindacato'))
        <span class="text-danger" >
            {{ $errors->first('sindacato') }}
        </span>
    @endif
</div>

<div class="form-group">
    <label for="federazione">Federazione</label>
    <input type="text" class="form-control" id="federazione" value="{{old('federazione',isset($record->federazione) ? $record->federazione : '')}}" name="federazione">
    @if($errors->has('federazione'))
        <span class="text-danger" >
            {{ $errors->first('federazione') }}
        </span>
    @endif
</div>

<div class="form-group">
    <label for="codice_sede">Codice Sede</label>
    <input type="text" class="form-control" id="codice_sede" value="{{old('codice_sede',isset($record->codice_sede) ? $record->codice_sede : '')}}" name="codice_sede">
    @if($errors->has('codice_sede'))
        <span class="text-danger" >
            {{ $errors->first('codice_sede') }}
        </span>
    @endif
</div>

<div class="form-group">
    <label for="sede_in_chiaro">Sede in chiaro</label>
    <input type="text" class="form-control" id="sede_in_chiaro" value="{{old('sede_in_chiaro',isset($record->sede_in_chiaro) ? $record->sede_in_chiaro : '')}}" name="sede_in_chiaro">
    @if($errors->has('sede_in_chiaro'))
        <span class="text-danger" >
            {{ $errors->first('sede_in_chiaro') }}
        </span>
    @endif
</div>

<div class="form-group">
    <label for="tipo_presentazione">Tipo presentazione</label>
    <input type="text" class="form-control" id="tipo_presentazione" value="{{old('tipo_presentazione',isset($record->tipo_presentazione) ? $record->tipo_presentazione : '')}}" name="tipo_presentazione">
    @if($errors->has('tipo_presentazione'))
        <span class="text-danger" >
            {{ $errors->first('tipo_presentazione') }}
        </span>
    @endif
</div>

<div class="form-group">
    <label for="nome_cognome">nome_cognome</label>
    <input type="text" class="form-control" id="nome_cognome" value="{{old('nome_cognome',isset($record->nome_cognome) ? $record->nome_cognome : '')}}" name="nome_cognome">
    @if($errors->has('nome_cognome'))
        <span class="text-danger" >
            {{ $errors->first('nome_cognome') }}
        </span>
    @endif
</div>

<div class="form-group">
    <label for="codice_fiscale">codice_fiscale</label>
    <input type="text" class="form-control" id="codice_fiscale" value="{{old('codice_fiscale',isset($record->codice_fiscale) ? $record->codice_fiscale : '')}}" name="codice_fiscale">
    @if($errors->has('codice_fiscale'))
        <span class="text-danger" >
            {{ $errors->first('codice_fiscale') }}
        </span>
    @endif
</div>

<div class="form-group">
    <label for="data_nascita">data_nascita</label>
    <input type="date" class="form-control" id="data_nascita" value="{{old('data_nascita',isset($record->data_nascita) ? $record->data_nascita->format("Y-m-d") : '')}}" name="data_nascita">
    @if($errors->has('data_nascita'))
        <span class="text-danger" >
            {{ $errors->first('data_nascita') }}
        </span>
    @endif
</div>

<div class="form-group">
    <label for="importo_trattenuta">importo_trattenuta</label>
    <input type="number" class="form-control" id="importo_trattenuta" value="{{old('importo_trattenuta',isset($record->importo_trattenuta) ? $record->importo_trattenuta : '')}}" name="importo_trattenuta">
    @if($errors->has('importo_trattenuta'))
        <span class="text-danger" >
            {{ $errors->first('importo_trattenuta') }}
        </span>
    @endif
</div>

<div class="form-group">
    <label for="data_valuta">data_valuta</label>
    <input type="date" class="form-control" id="data_valuta" value="{{old('data_valuta',isset($record->data_valuta) ? $record->data_valuta->format("Y-m-d") : '')}}" name="data_valuta">
    @if($errors->has('data_valuta'))
        <span class="text-danger" >
            {{ $errors->first('data_valuta') }}
        </span>
    @endif
</div>

<div class="form-group">
    <label for="data_pagamento">data_pagamento</label>
    <input type="date" class="form-control" id="data_pagamento" value="{{old('data_pagamento',isset($record->data_pagamento) ? $record->data_pagamento->format("Y-m-d") : '')}}" name="data_pagamento">
    @if($errors->has('data_pagamento'))
        <span class="text-danger" >
            {{ $errors->first('data_pagamento') }}
        </span>
    @endif
</div>

<div class="form-group">
    <label for="numero_domande">numero_domande</label>
    <input type="number" class="form-control" id="numero_domande" value="{{old('numero_domande',isset($record->numero_domande) ? $record->numero_domande : '')}}" name="numero_domande">
    @if($errors->has('numero_domande'))
        <span class="text-danger" >
            {{ $errors->first('numero_domande') }}
        </span>
    @endif
</div>

<div class="form-group">
    <label for="comune_residenza">comune_residenza</label>
    <input type="text" class="form-control" id="comune_residenza" value="{{old('comune_residenza',isset($record->comune_residenza) ? $record->comune_residenza : '')}}" name="comune_residenza">
    @if($errors->has('comune_residenza'))
        <span class="text-danger" >
            {{ $errors->first('comune_residenza') }}
        </span>
    @endif
</div>

<div class="form-group">
    <label for="codice_patronato">codice_patronato</label>
    <input type="text" class="form-control" id="codice_patronato" value="{{old('codice_patronato',isset($record->codice_patronato) ? $record->codice_patronato : '')}}" name="codice_patronato">
    @if($errors->has('codice_patronato'))
        <span class="text-danger" >
            {{ $errors->first('codice_patronato') }}
        </span>
    @endif
</div>

<div class="form-group">
    <label for="ufficio_patronato">ufficio_patronato</label>
    <input type="text" class="form-control" id="ufficio_patronato" value="{{old('ufficio_patronato',isset($record->ufficio_patronato) ? $record->ufficio_patronato : '')}}" name="ufficio_patronato">
    @if($errors->has('ufficio_patronato'))
        <span class="text-danger" >
            {{ $errors->first('ufficio_patronato') }}
        </span>
    @endif
</div>

<div class="form-group">
    <label for="patronato">patronato</label>
    <input type="text" class="form-control" id="patronato" value="{{old('patronato',isset($record->patronato) ? $record->patronato : '')}}" name="patronato">
    @if($errors->has('patronato'))
        <span class="text-danger" >
            {{ $errors->first('patronato') }}
        </span>
    @endif
</div>
