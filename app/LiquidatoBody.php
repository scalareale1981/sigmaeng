<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiquidatoBody extends Model
{
    //


    public function attachable()
    {
        return $this->morphTo();
    }
}
