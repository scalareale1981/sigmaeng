<?php

namespace App\Providers;

use App\TipoSede;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\User;
use Illuminate\Support\Facades\DB;


class ViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('widgets.formSede', function ($view) {
            $users = User::where('category_id',3)->get();
            $view->with('users',$users);
            $tipo_sedi = TipoSede::all();
            $view->with('tipo_sedi',$tipo_sedi);
        });

        View::composer('widgets.formPagamento', function ($view) {
            $responsabili = DB::table('sedi')
                        ->join('users','sedi.responsabile_id','=','users.id')
                        ->select('users.*')
                        ->orderBy('users.cognome')
                        ->orderBy('users.nome')
                        ->groupBy('users.id')
                        ->get();
            $view->with('responsabili',$responsabili);
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
