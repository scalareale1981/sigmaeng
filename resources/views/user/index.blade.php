@extends('layouts.dashboardAdmin')
@section('page_heading','Utenti')
@section('section')

    <div class="row" style="padding-top: 10px; padding-bottom: 10px;">
        <div class="col-sm-11 text-left">
            <form>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search" name="keywords">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-1 text-right">
            <a href="{{url('/admin/user/create')}}" class="btn btn-success"><i class="fa fa-plus"></i></a>
        </div>
    </div>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Nome</th>
            <th>Cognome</th>
            <th>E-Mail</th>
            <th>Categoria</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
            @forelse($users as $user)
                <tr>
                    <td>{{$user->nome}}</td>
                    <td>{{$user->cognome}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->category->nome}}</td>
                    <td>
                        <button type="button" class="btn btn-info show-btn"  data-user-id="{{$user->id}}"><i class="fa fa-info"></i></button>
                        <a href="{{url('/admin/user/edit/'.$user->id)}}" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="20" class="table-warning">Nessun record</td>
                </tr>
            @endforelse
        </tbody>
    </table>
    {{ $users->links() }}
    <!-- Modal -->
    <div id="userShow" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Dettaglio Utente</h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <a href="" class="btn btn-warning" id="mod-user"><i class="fa fa-pencil"></i> Modifica</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Chiudi</button>
                </div>
            </div>

        </div>
    </div>
@stop
@section('script')
    <script>

        $(".show-btn").click(function(e){
            $("#userShow").find(".modal-body").html("<div class='text-center'><i class=\"fa fa-spinner fa-spin fa-3x fa-fw\"></i></div>");
            var userId = $(this).data('userId');

            $("#userShow").modal();
            $.ajax({
                method: "POST",
                url: "{{url('admin/user/ajax/show')}}",
                data: { _token:'{{csrf_token()}}',id: userId }
            })
                .done(function( msg ) {
                    $("#userShow").find(".modal-body").html(msg);
                    $("#mod-user").attr('href','{{url('/admin/user/edit/')}}/'+userId);
                });
        });

    </script>
@stop
