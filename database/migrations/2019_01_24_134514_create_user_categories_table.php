<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\UserCategory;

class CreateUserCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->timestamps();
        });

        $UserCategory = new UserCategory();
        $UserCategory->nome = 'Amministratore';
        $UserCategory->save();
        $UserCategory = new UserCategory();
        $UserCategory->nome = 'Utente';
        $UserCategory->save();
        $UserCategory = new UserCategory();
        $UserCategory->nome = 'Addetto';
        $UserCategory->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_categories');
    }
}
