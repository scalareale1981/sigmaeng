<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AddettoController extends Controller
{
    public function dashboard(){
        return view('addetto.dashboard');
    }

    public function responsabile(){

        $responsabilita = Auth::user()->responsabile()->paginate(20);


        return view('addetto.responsabile',compact('responsabilita'));
    }

    public function referente(){

        $referenze = Auth::user()->referente()->paginate(20);


        return view('addetto.referente',compact('referenze'));
    }
}
