@extends('layouts.dashboardAdmin')
@section('page_heading','Amministrazione Addetti')
@section('section')
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Nome</th>
            <th>Cognome</th>
            <th>E-Mail</th>
            <th>Referente</th>
            <th>Quota</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @forelse($records as $record)
            <tr>
                <td>{{$record->nome}}</td>
                <td>{{$record->cognome}}</td>
                <td>{{$record->email}}</td>
                <td>{{$record->referente}}</td>
                <td>{{$record->quota}}</td>
                <td>
                    <a href="" class="btn btn-info"><i class="fa fa-info"></i></a>
                    <a href="" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                </td>
            </tr>
        @empty
            <tr class="table-warning">
                <td colspan="20" >Nessun record</td>
            </tr>
        @endforelse
        </tbody>
    </table>
    {{ $records->links() }}
@stop
