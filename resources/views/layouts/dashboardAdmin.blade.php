@extends('layouts.plane')

@section('body')
    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" style="padding:0px;" href="{{ url ('') }}">
                    <img src="{{ url ('img/logo.png') }}" title="logo" style="height: 45px;">
                </a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li {{ (Request::is('/') ? 'class=active' : '') }}>
                            <a href="{{ url ('/admin/home') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li {{ (Request::is('*/user*') ? 'class=active' : '') }}>
                            <a href="{{ url ('/admin/users') }}"><i class="fa fa-users fa-fw"></i> Utenti</a>
                        </li>
                        <li {{ (Request::is('*/sed*') ? 'class=active' : '') }}>
                            <a href="{{ url ('/admin/sedi') }}"><i class="fa fa-home fa-fw"></i> Sedi</a>
                        </li>
                        <li {{ (Request::is('*/record*') ? 'class=active' : '') }}>
                            <a href="{{ url ('/admin/records') }}"><i class="fa fa-edit fa-fw"></i> Records</a>
                        </li>
                        <li {{ (Request::is('*/pagament*') ? 'class=active' : '') }}>
                            <a href="{{ url ('/admin/pagamenti') }}"><i class="fa fa-dollar fa-fw"></i> Acconti</a>
                        </li>
                        <li {{ (Request::is('*/liquida*') ? 'class=active' : '') }}>
                            <a href="{{ url ('/admin/liquidazioni') }}"><i class="fa fa-dollar fa-fw"></i> Liquidazioni</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">@yield('page_heading')</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                @yield('section')

            </div>
            <!-- /#page-wrapper -->
        </div>
    </div>
@stop

