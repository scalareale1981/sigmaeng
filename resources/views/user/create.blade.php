@extends('layouts.dashboardAdmin')
@section('page_heading','Crea nuovo utente')
@section('section')
    <div class="form-group">
        <label for="categoryid">Categoria</label>
        <select id="categoryid" class="form-control">
            <option value="">--</option>
            @foreach($categories as $categorie)
                <option value="{{$categorie->id}}">{{$categorie->nome}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-12 form-area" id="form_1" style="display: none;">
        <form action="{{url('/admin/user/store')}}" method="post">
            @csrf
            @include('widgets.formAmministratore')
            <button type="submit" class="btn btn-primary">Salva</button>
        </form>
    </div>
    <div class="col-12 form-area" id="form_2" style="display: none;">
        <form action="{{url('/admin/user/store')}}" method="post">
            @csrf
            @include('widgets.formUtente')
            <button type="submit" class="btn btn-primary">Salva</button>
        </form>
    </div>
    <div class="col-12 form-area" id="form_3" style="display: none;">
        <form action="{{url('/admin/user/store')}}" method="post">
            @csrf
            @include('widgets.formAddetto')
            <button type="submit" class="btn btn-primary">Salva</button>
        </form>
    </div>
    <div style="margin:10px;">&nbsp;</div>
@stop
@section('script')
    <script defer>
        $(function(){
            $("#categoryid").change(function(){
                $(".form-area").hide();
                var category = $(this).val();
                if($("#form_"+category).length){
                    $("#form_"+category).show();
                }
            });
            @if(old('category_id'))
                $("#form_{{old('category_id')}}").show();
                $("#categoryid").val("{{old('category_id')}}");
            @endif
        })
    </script>
@stop
