@extends('layouts.dashboardAdmin')
@section('page_heading','Aggiungi acconto')
@section('section')
    <div class="col-12 form-area" id="form">
        <form action="{{url('/admin/pagamento/store')}}" method="post">
            @csrf
            @include('widgets.formPagamento')
            <button type="submit" class="btn btn-primary">Salva</button>
        </form>
    </div>
    <div style="margin:10px;">&nbsp;</div>
@stop
