<?php

namespace App\Http\Controllers;

use App\Http\Requests\SedeRequest;
use App\Sede;
use App\TipoSede;
use Illuminate\Http\Request;

class SedeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('sede.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SedeRequest $request)
    {
        //
        $sede = new Sede();
        $sede->denominazione = $request->denominazione;
        $sede->indirizzo = $request->indirizzo;
        $sede->citta = $request->citta;
        $sede->provincia = $request->provincia;
        $sede->codice = $request->codice;
        $sede->tipo_sede_id = $request->tipo_sede_id;
        $sede->responsabile_id = $request->responsabile_id;
        $sede->responsabile_quota = $request->responsabile_quota;
        $sede->referente_id = $request->referente_id;
        $sede->referente_quota = $request->referente_quota;
        $sede->save();

        return redirect('/admin/sedi');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sede = Sede::find($id);
        return $sede;
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ajaxShow(Request $request)
    {
        $id = $request->id;
        $sede = Sede::find($id);

        return view('sede.ajax.show',compact('sede'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Sede $sede)
    {
        return view('sede.edit',compact('sede'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SedeRequest $request, Sede $sede)
    {
        //
        $sede->denominazione = $request->denominazione;
        $sede->indirizzo = $request->indirizzo;
        $sede->citta = $request->citta;
        $sede->provincia = $request->provincia;
        $sede->codice = $request->codice;
        $sede->tipo_sede_id = $request->tipo_sede_id;
        $sede->responsabile_id = $request->responsabile_id;
        $sede->responsabile_quota = $request->responsabile_quota;
        $sede->referente_id = $request->referente_id;
        $sede->referente_quota = $request->referente_quota;
        $sede->save();

        return redirect('/admin/sede/edit/'.$sede->id)->with('status','Modifiche effettuate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
