<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //dd($this);

        if (!isset($this->user_id)){
            return [
                'nome' => 'required|string|max:50',
                'cognome' => 'required|string|max:50',
                'codice_fiscale' => 'nullable|regex:/^[A-Za-z]{6}[0-9LMNPQRSTUV]{2}[A-Za-z]{1}[0-9LMNPQRSTUV]{2}[A-Za-z]{1}[0-9LMNPQRSTUV]{3}[A-Za-z]{1}$/i',
                'provincia' => 'nullable|regex:/^[A-Za-z]{2}$/i',
                'email' => 'required|email|unique:users',
                'password' => 'required',
                'data_di_nascita' => 'nullable|date_format:Y-m-d'
            ];
        }
        else{
            return [
                'nome' => 'required|string|max:50',
                'cognome' => 'required|string|max:50',
                'codice_fiscale' => 'nullable|regex:/^[A-Za-z]{6}[0-9LMNPQRSTUV]{2}[A-Za-z]{1}[0-9LMNPQRSTUV]{2}[A-Za-z]{1}[0-9LMNPQRSTUV]{3}[A-Za-z]{1}$/i',
                'provincia' => 'nullable|regex:/^[A-Za-z]{2}$/i',
                'email' => 'required|email|unique:users,email,'.$this->user_id,
                'data_di_nascita' => 'nullable|date_format:Y-m-d'
            ];
        }
    }

    public function messages()
    {
        return [
            'nome.required' =>'Campo obbligatorio',
            'cognome.required' =>'Campo obbligatorio',
            'codice_fiscale.min' =>'Inserire un codice fiscale valido',
            'codice_fiscale.max' =>'Inserire un codice fiscale valido',
            'provincia.max' =>'Inserire il codice',
            'email.required' =>'Campo obbligatorio',
            'email.email' =>'Inserire un indirizzo valido',
            'email.unique' =>'Indirizzo già utilizzato',
            'password.required' => 'Campo obbligatorio',
            'codice_fiscale.regex' => 'Inserire un codice fiscale valido',
            'data_di_nascita.date_format' => 'Formato data non valido'
        ];
    }

}
