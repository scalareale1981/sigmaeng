@extends('layouts.dashboardAdmin')
@section('page_heading','Sedi')
@section('section')

    <div class="row" style="padding-top: 10px; padding-bottom: 10px;">
        <div class="col-sm-11 text-left">
            <form>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search" name="keywords">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-sm-1 text-right">
            <a href="{{url('/admin/sede/create')}}" class="btn btn-success"><i class="fa fa-plus"></i></a>
        </div>
    </div>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Denominazione</th>
            <th>Codice</th>
            <th>Tipo Sede</th>
            <th>Referente</th>
            <th>Responsabile</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
            @forelse($sedi as $sede)
                <tr>
                    <td>{{$sede->denominazione}}</td>
                    <td>{{$sede->codice}}</td>
                    <td>
                        {{$sede->tipo_sede->descrizione}}
                    </td>
                    <td>
                        @if($sede->referente)
                            {{$sede->referente->nome}} {{$sede->referente->cognome}} - {{$sede->referente->email}}
                        @endif
                    </td>
                    <td>
                        @if($sede->responsabile)
                            {{$sede->responsabile->nome}} {{$sede->responsabile->cognome}} - {{$sede->responsabile->email}}
                        @endif
                    </td>
                    <td>
                        <button type="button" class="btn btn-info show-btn" data-sede-id="{{$sede->id}}"><i class="fa fa-info"></i></button>
                        <a href="{{url('/admin/sede/edit/'.$sede->id)}}" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="20" class="table-warning">Nessun record</td>
                </tr>
            @endforelse
        </tbody>
    </table>
    {{ $sedi->links() }}

    <!-- Modal -->
    <div id="sedeShow" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Dettaglio sede</h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <a href="" class="btn btn-warning" id="mod-sede"><i class="fa fa-pencil"></i> Modifica</a>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Chiudi</button>
                </div>
            </div>

        </div>
    </div>
@stop
@section('script')
    <script>

        $(".show-btn").click(function(e){
            $("#sedeShow").find(".modal-body").html("<div class='text-center'><i class=\"fa fa-spinner fa-spin fa-3x fa-fw\"></i></div>");
            var sedeId = $(this).data('sedeId');

            $("#sedeShow").modal();
            $.ajax({
                method: "POST",
                url: "{{url('admin/sede/ajax/show')}}",
                data: { _token:'{{csrf_token()}}',id: sedeId }
            })
            .done(function( msg ) {
                $("#sedeShow").find(".modal-body").html(msg);
                $("#mod-sede").attr('href','{{url('/admin/sede/edit/')}}/'+sedeId);
            });

        });

    </script>
@stop
