<?php

namespace App\Http\Controllers;

use App\Http\Requests\PagamentoRequest;
use App\Pagamento;
use Illuminate\Http\Request;
use App\User;

class PagamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pagamento.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PagamentoRequest $request)
    {
        switch($request->type){
            case 'user':
                $user = User::find($request->id);
                $p = new Pagamento();
                $p->importo = $request->importo;
                $p->data_pagamento = $request->data_pagamento;
                $p->nota = $request->nota;
                $user->pagamenti()->save($p);
            break;
        }
        return redirect('admin/pagamenti')->with('status','Pagamento registrato con successo');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pagamento  $pagamento
     * @return \Illuminate\Http\Response
     */
    public function show(Pagamento $pagamento)
    {
        //
    }

    public function ajaxShow(Request $request)
    {
        $pagamento = Pagamento::find($request->id);
        return view('pagamento.ajax.show',compact('pagamento'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pagamento  $pagamento
     * @return \Illuminate\Http\Response
     */
    public function edit(Pagamento $pagamento)
    {

        return view('pagamento.edit',compact('pagamento'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pagamento  $pagamento
     * @return \Illuminate\Http\Response
     */
    public function update(PagamentoRequest $request, Pagamento $pagamento)
    {
        switch($request->type){
            case 'user':
                $user = User::find($request->id);
                $pagamento->importo = $request->importo;
                $pagamento->data_pagamento = $request->data_pagamento;
                $pagamento->nota = $request->nota;
                $user->pagamenti()->save($pagamento);
                break;
        }
        return redirect('admin/pagamento/edit/'.$pagamento->id)->with('status','Pagamento modificato');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pagamento  $pagamento
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pagamento $pagamento)
    {
        //
    }
}
