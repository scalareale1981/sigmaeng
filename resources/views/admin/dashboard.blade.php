@extends('layouts.dashboardAdmin')
@section('page_heading','Admin Dashboard')
@section('head')
    <link rel="stylesheet" href="{{url('js/tablesorter-master/addons/pager/jquery.tablesorter.pager.css')}}">
    <link rel="stylesheet" href="{{url('js/tablesorter-master/css/theme.blue.css')}}">
    <style>
        .cell_padding_1{
            padding:2px !important;
        }

        .pagination a{
            padding:6px 6px !important;
        }
    </style>
@endsection
@section('section')

    <div class="row">
        <div class="col-sm-12 riepilogo">
            <div class="table-responsive">
                <table class="tablesorter table table-striped">
                    <thead>
                        <tr>
                            <th class="select-find">Responsabile</th>
                            <th class="select-find">Referente</th>
                            <th class="select-find">Sede</th>
                            <th>Codice</th>
                            <th class="select-find">Tipo Sede</th>
                            <th>Quote</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th class="input-find"></th>
                            <th></th>
                            <th class="container_quote"></th>
                        </tr>
                    </tfoot>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop

@section('script')

    <link href="{{url('js/datatables.css')}}" rel="stylesheet"/>
    <script src="{{url('js/datatables.js')}}"></script>
    <script>
        $(document).ready(function() {
            var table = $('.tablesorter').DataTable({
                "ajax": '{{url('admin/ajax/riepilogo/quote')}}',
                "language": {
                    "sEmptyTable":     "Nessun dato presente nella tabella",
                    "sInfo":           "Da _START_ a _END_ di _TOTAL_",
                    "sInfoEmpty":      "Da 0 a 0 di 0",
                    "sInfoFiltered":   "(filtrati da _MAX_ elementi totali)",
                    "sInfoPostFix":    "",
                    "sInfoThousands":  ",",
                    "sLengthMenu":     "Visualizza _MENU_ elementi",
                    "sLoadingRecords": "Caricamento...",
                    "sProcessing":     "Elaborazione...",
                    "sSearch":         "Cerca:",
                    "sZeroRecords":    "La ricerca non ha portato alcun risultato.",
                    "oPaginate": {
                        "sFirst":      "Inizio",
                        "sPrevious":   "<<",
                        "sNext":       ">>",
                        "sLast":       "Fine"
                    },
                    "oAria": {
                        "sSortAscending":  ": attiva per ordinare la colonna in ordine crescente",
                        "sSortDescending": ": attiva per ordinare la colonna in ordine decrescente"
                    }
                },
                "columns": [
                    { className: "cell_padding_1" },
                    { className: "cell_padding_1" },
                    { className: "cell_padding_1" },
                    { className: "cell_padding_1" },
                    { className: "cell_padding_1" },
                    { className: "cell_padding_1 cell_quota text-center" }
                ],
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                initComplete: function () {
                    this.api().columns('.select-find').every( function () {
                        var column = this;
                        var select = $('<select class="form-control" style="width:100%; padding:1px; height: auto;"><option value=""></option></select>')
                            .appendTo( $(column.footer()).empty() )
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );
                                column
                                    .search( val ? '^'+val+'$' : '', true, false )
                                    .draw();
                            });

                        column.data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        });
                    } );
                    sum_quote();

                    $('.tablesorter tfoot .input-find').each( function () {
                        var title = $(this).text();
                        $(this).html( '<input type="text" class="form-control" style="width:100%; padding:0px; height: auto;" '+title+'" />' );
                    } );

                    this.api().columns().every( function () {
                        var that = this;
                        $( 'input', this.footer() ).on( 'keyup change', function () {
                            if ( that.search() !== this.value ) {
                                that
                                    .search( this.value )
                                    .draw();
                            }
                        } );
                    } );

                }
            });





        });




        function sum_quote(){
            $(".container_quote").html(0);
            var quote = 0;
            $('.tablesorter').find('tbody').find(".cell_quota").each(function(){
                var value = +$(this).html();
                quote += value;
            });
            $(".container_quote").html(quote);
        }

        $(".riepilogo").on('change',function () {
            sum_quote();

        });

        $(".riepilogo").on('click',function () {
            sum_quote();
        });

    </script>
@stop
